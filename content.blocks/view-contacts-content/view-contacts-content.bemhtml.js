block('view-contacts-content').content()([
    {
        tag: 'h1',
        content: 'Контакты'
    },
    {
        block: 'view',
        elem: 'wrapper',
        content: [
            {
                tag: 'h4',
                content: 'Служба поддержки клиентов:'
            },
            {
                elem: 'info',
                content: [
                    {
                        elem: 'location',
                        tag: 'span',
                        content: 'Москва:'
                    },
                    {
                        block: 'link',
                        mix: {block: 'view-contacts-content', elem: 'phone-number'},
                        url: 'tel:8(495)2151198',
                        content: '8 (495) 215-11-98'
                    }
                ]
            },
            {
                elem: 'info',
                content: [
                    {
                        elem: 'location',
                        tag: 'span',
                        content: 'Россия:'
                    },
                    {
                        block: 'link',
                        mix: {block: 'view-contacts-content', elem: 'phone-number'},
                        url: 'tel:8(800)5552198',
                        content: '8 (800) 555-21-98'
                    },
                    {
                        elem: 'free-call',
                        content: '(звонок бесплатный)'
                    }
                ]
            }
        ]
    },
    {
        block: 'view',
        elem: 'wrapper',
        content: [
            {
                tag: 'h4',
                content: 'Ваши предложения и партнерство:'
            },
            {
                elem: 'info',
                content: {
                    block: 'link',
                    mix: {block: 'view-contacts-content', elem: 'phone-number'},
                    url: 'tel:8(495)5858881',
                    content: '8 (495) 585-88-81'
                }
            }
        ]
    },
    {
        block: 'view',
        elem: 'wrapper',
        content: [
            {
                tag: 'h4',
                content: 'Адрес:'
            },
            {
                elem: 'info',
                content: '121357, г.Москва, ул. Козлова, д.30'
            }
        ]
    }
]);
