/** List of technologies, like in bem-components */
module.exports = {
    files : {
        provide : require('enb/techs/file-provider'),
        merge : require('enb/techs/file-merge')
        // copy : require('enb/techs/file-copy')
        // write : require('enb/techs/write-file')
    },
    bem: require('enb-bem-techs'),
    stylus: require('enb-stylus/techs/stylus'),
    browserJs: require('enb-js/techs/browser-js'),
    bemhtml : require('enb-bemxjst/techs/bemhtml'),
    bemjsonToHtml: require('enb-bemxjst/techs/bemjson-to-html'),
    borschik : require('enb-borschik/techs/borschik')
};
