// http://eslint.org/docs/user-guide/configuring
// docs for rules at http://eslint.org/docs/rules/{ruleName}
module.exports = {
    "env": {
        "browser": true,
        "commonjs": true,
        "mocha": true,
        "es6": false,
        "node": true,
        "bem-xjst/bemhtml": true
    },
    "extends": [
        "eslint:recommended",
        "airbnb-base"
    ],
    "plugins": [
        // https://github.com/bem/eslint-plugin-bem-xjst
        "bem-xjst"
    ],
    "globals": {
        // ymodules global variable
        // todo: find a ymodules plugin
        //   or change a module system, eg. from ymodules to commonjs
        "modules": true
    },
    "rules": {
        // http://eslint.org/docs/rules/brace-style
        "indent": [
            "error",
            4
        ],
        "linebreak-style": [
            "error",
            "unix"
        ],
        "quotes": [
            "error",
            "single"
        ],
        "semi": [
            "error",
            "always"
        ],
        "arrow-body-style": "off",
        "valid-jsdoc": "warn",
        "eol-last": "warn",
        "key-spacing": "off",
        "comma-dangle": "off",
        "object-shorthand": "off",
        "global-require": "off",
        "func-names": "off",
        "prefer-arrow-callback": "off",
        "padded-blocks": "warn",
        "prefer-template": "off",
        "vars-on-top": "off",
        "max-len": "off",
        "quote-props": "off",
        "prefer-rest-params": "off",
        // deps.js without expressions
        "no-unused-expressions": "off",
        "no-underscore-dangle": "off",
        "no-var": "off",
        "no-param-reassign": "warn",
        "no-console": "off",
        "spaced-comment": "off",
        "space-before-function-paren": "off",
        "import/no-extraneous-dependencies": [
            "warn",
            { "devDependencies": true }
        ],
        "no-restricted-syntax": [
            "error",
            "ArrowFunctionExpression",
            "ClassExpression"
        ]
    }
};
