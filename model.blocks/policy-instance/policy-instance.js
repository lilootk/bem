/**
 * Если есть данные в адресной строке:
 * - извлечь из хэша
 * - подписаться (если нужен синхронный стэйт в адресной строке)
 *
 * Если доступно локальное хранилище
 * - извлечь из хранилище
 * - подписаться
 *
 * Также возможен вариант:
 * - взять параметры из юрл, а сохранять в локальное хранилище
*/
modules.define('policy-instance', [
    'localforage',
    'functions__debounce',
    'jquery',
    'policy-state',
    'dates'
], function(provide,
            localforage,
            debounce,
            $,
            PolicyState,
            dates) {
    // Разница в минутах от UTC; getTimezoneOffset()
    var UTC_OFFSET = -180;  // MSK
    // Бэкап-хранилище
    var BACKUP_DB = 'che_db';
    var BACKUP_STORE = 'che_store12';
    var BACKUP_KEY = 'che_model';
    // milliseconds
    var BACKUP_DELAY = 500;

    var COUNTRIES_ENDPOINT = 'https://d2j2dl4huu79en.cloudfront.net/j/countries.json';

    /**
     * Асинхронная подгрузка списка стран и других асинк данных
     * добавление в уже готовую модель
     * @returns {ComputedState} synchronously
     */
    var attachAsyncData = function(policyStateStorage) {
        $.getJSON(COUNTRIES_ENDPOINT, function(result) {
            policyStateStorage.update({
                originCountries: result
            });
        });

        return policyStateStorage;
    };

    /**
     * Если произошла ошибка при чтении бэкапа (например при запрете)
     * - то ошибка игнорируется и возвращается пустое значение
     * - либо можно уведомить пользователя включить бэкап
     * @private
     */
    var catchReadBackup = function(backupStorageError) {
        console.log('Backup is not available', backupStorageError);
        return null;
    };

    var fixOldDate = function(dateTarget, dateInitial) {
        if (!dateTarget) { return null; }

        if (dates.fromGostToStamp(dateTarget) < dates.fromGostToStamp(dateInitial)) {
            // return null or dateInitial
            return null;
        }

        return dateTarget;
    };

    // Модель может быть создана:
    // - из бэкапа
    // - из параметров адресной строки
    // в скопе с параметрами по умолчанию
    // Преобразование из бэкапа в структуру модели
    // Фиксация устаревших дат (отдельно)
    var buildInitialState = function(backupState, dateInitial) {
        var initialState = backupState || {};
        initialState.tripRange = initialState.tripRange || {};

        initialState.tripRange.initial = dateInitial;
        initialState.tripRange.maxDuration = 'P1Y-1D';
        // по умолчанию обычный полис
        initialState.tripRange.isFixed = initialState.tripRange.isFixed || false;

        initialState.tripRange.start = fixOldDate(initialState.tripRange.start);

        initialState.tripRange.end = fixOldDate(initialState.tripRange.end);

        return initialState;
    };

    /**
     * Создание бэкап-версии из полиса и сохранение.
     * Бэкап-версия не содержит сведения о всей модели, только необходимые поля, позволяющие восстановить состояние.
     * Ошибки сохранения показываются, но игнорируются
     * Хранилище может быть включено в процессе пользовательского взаимодействия, тогда бэкап заработает автоматически
     */
    var subscribeToBackup = function(backupStorage, policyStateStorage) {
        var backup = function(changedKeys, entireState, writableState) {
            delete writableState.originCountries;
            delete writableState.viewUrl;

            backupStorage.setItem(BACKUP_KEY, writableState)
                .then(function(value) {
                    console.log('backup is done', JSON.stringify(value));
                })
                .catch(function(err) {
                    console.log('backup storage is not available: skip it', err);
                });
        };

        var backupLazy = debounce(backup, BACKUP_DELAY);
        // Сохранение в бэкап при модификации состояния (полиса)
        policyStateStorage.subscribe(backupLazy);
        // TODO: use specific flags for backuped props
        return policyStateStorage;
    };

    var createStore = function(backupState) {
        var dateInitial = dates.offsetDate(new Date(), UTC_OFFSET);
        var initialState = buildInitialState(backupState, dateInitial);

        var policyStateStorage = new PolicyState();

        try {
            policyStateStorage.update(initialState);
        } catch (exc) {
            console.log('Ошибка: новая структура данных не совмещена со старой: бэкап данные обнулены');
            var emptyState = buildInitialState(null, dateInitial);
            policyStateStorage.update(emptyState);
        }

        return policyStateStorage;
    };

    var init = function() {
        // создание экземпляра бэкап-хранилища
        var backupStorage = localforage.createInstance({
            name: BACKUP_DB,
            storeName: BACKUP_STORE
        });

        return backupStorage.getItem(BACKUP_KEY)
            .catch(catchReadBackup)
            .then(createStore)
            .then(subscribeToBackup.bind(null, backupStorage))
            .then(attachAsyncData);
    };

    provide({ init: init });
});
