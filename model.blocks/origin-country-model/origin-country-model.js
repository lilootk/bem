modules.define('origin-country-model', function (provide) {
    var model = {
        // estonia, finland, england
        id: { type: String },
        name: { type: String },
        // "group" or "country"
        group: { type: String },
        // данное поле является вычисляемым, пока источник явно не предоставит данного поля
        isGroup: {
            type: Boolean,
            computed: ['group', function(group) {
                if (group === null) { return null; }
                return group === 'group';
            }]
        },
        search: { type: String },
        // на данный момент эти поля вычисляемые, так как нет нужных данных в источнике, например {id:italy, isDateVisaRequired: true, isShengen: true}
        // при появлении этих полей в источнике - данные будут подгружаться напрямую без вычислений
        isDateVisaRequired: {
            type: Boolean,
            computed: ['id', function(id) {
                return id === 'estonia' || id === 'finland';
            }]
        },
        isShengen: {
            type: Boolean,
            computed: ['id', function(id) {
                return id === 'italy' || id === 'spain';
            }]
        }
    };

    provide(model);
});
