modules.define('insurer-model', ['matcher', 'dates'], function(provide, matcher, dates) {
    var model = {
        /** Дата отсчёта, необходимая для birthday min max */
        initial: { type: String },

        /**
         * имя страхователя латиницей
         * обязательно для страховых с useLocalName=0 в таблице companies
         * ограничения: только латинские символы пробел и дефис максимум 50 символов
         */
        name: { type: String },
        isNameValid: {
            type: Boolean,
            computed: ['name', matcher.isLatinName]
        },

        lastName: { type: String },
        isLastNameValid: {
            type: Boolean,
            computed: ['lastName', matcher.isLatinName]
        },

        /**
         * имя страхователя кирилицей (обязательно для страховых с useLocalName=1 в таблице companies, ограничения только русские символы пробел и дифис максимум 65535 символов (тип TEXT в базе))
         */
        nameRU: { type: String },
        lastNameRU: { type: String },
        secondNameRU: { type: String },

        /** дата, такая чтобы возраст был от 18 до 120 лет */
        birthday: { type: String },
        /** от 120 лет */
        birthdayMin: {
            type: String,
            computed: ['initial', matcher.minBirthday]
        },
        /** до 18 лет: initial - 18 years */
        birthdayMax: {
            type: String,
            computed: ['initial', matcher.maxAdultBirthday]
        },
        isBirthdayValid: {
            type: Boolean,
            computed: ['birthday', 'birthdayMin', 'birthdayMax',
                       dates.isDateBetweenInclusive]
        }
    };

    provide(model);
});

// email (<=50)
// phone (<=255)
