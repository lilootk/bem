modules.define('date-range-model', ['matcher', 'dates', 'duration-helper'], function(provide, matcher, dates, durationHelper) {
    var model = {
        /**
         * Дата отсчёта, ГОСТ
         * например, сегодня
         */
        initial: { type: String },

        /**
         * Дата начала и окончания поездки (путешествия), ГОСТ
         */
        start: { type: String },
        end: { type: String },

        /**
         * Максимально возможная продолжительность поездки (путешествия)
         * Формат: P1Y2M3D
         */
        maxDuration: { type: String },

        /** UTC timestamps */
        initialStamp: { type: Number, computed: ['initial', dates.fromGostToStamp] },
        startStamp: { type: Number, computed: ['start', dates.fromGostToStamp] },
        endStamp: { type: Number, computed: ['end', dates.fromGostToStamp] },

        /** eg: year later from a current Moscow date */
        latestInitial: {
            type: String,
            computed: ['initialStamp', 'maxDuration', durationHelper.calculateStampPlusDuration]
        },

        latestInitialStamp: {
            type: Number,
            computed: ['latestInitial', dates.fromGostToStamp]
        },

        /** eg: year later from a start date */
        latestStart: {
            type: String,
            computed: ['startStamp', 'maxDuration', durationHelper.calculateStampPlusDuration]
        },

        latestStartStamp: {
            type: Number,
            computed: ['latestStart', dates.fromGostToStamp]
        },

        /** Min limit for a start date */
        minStart: {
            type: String,
            computed: ['initial', function(initial) { return initial; }]
        },

        /**
         * Max limit for a start date, inclusive
         * minStart <= start <= maxStart
         */
        maxStart: {
            type: String,
            computed: ['latestInitialStamp', 'endStamp', function(latestInitialStamp, endStamp) {
                if (latestInitialStamp !== null) {
                    if (endStamp !== null && endStamp < latestInitialStamp) {
                        return dates.fromStampToGost(endStamp);
                    }

                    return dates.fromStampToGost(latestInitialStamp);
                }

                return null;
            }]
        },

        /** Min limit for an end date */
        minEnd: {
            type: String,
            computed: ['initial', 'start', matcher.lastAvailable]
        },

        /**
         * Max limit for an end date
         * minEnd <= end < maxEnd
         */
        maxEnd: {
            type: String,
            computed: ['latestInitial', 'latestStart', matcher.lastAvailable]
        },

        /** Number of days between start and end dates, including start and end days */
        daysBetween: {
            type: Number,
            computed: ['endStamp', 'startStamp', dates.diffDaysBetweenStamps]
        },

        isStartValid: {
            type: Boolean,
            computed: ['start', 'minStart', 'maxStart', dates.isDateBetweenInclusive]
        },

        isEndValid: {
            type: Boolean,
            computed: ['end', 'minEnd', 'maxEnd', dates.isDateBetweenInclusive]
        }
    };

    provide(model);
});
