/**
 * Турист
 * Для калькуляции необходимо указать минимум одного туриста: возраст
 * Для оплаты - минимум одного туриста (все данные)
 *
 * Если нужны 10 заготовок, то создаётся массив с 10-ю null записями
 * - при клике true на галочку: update tourists[3] = {}
 * - при false: update tourists[3] = null
 * @module
 */
modules.define('tourist-model', ['matcher', 'dates'], function(provide, matcher, dates) {
    var model = {
        /**
         * Порядковый номер
         *
         * Вариант без идентификатора:
         * - добавление, удаление туристов происходит по порядку
         *   - нельзя удалить 3-го туриста без удаления всех последующих (либо обнуление)
         *   - нельзя добавить 4-го туриста без добавления предыдущих (либо обнуление всех предыдущих)
         * - операции обновления происходят по индексу в массиве туристов: при одновременных операциях удаления и обновления возможен сдвиг индекса.
         */
        id: { type: Number },

        /**
         * Дата отсчёта, сегодняшний день
         * Необходима для вычисления дат рождений, доступных для выбора
         */
        initial: { type: String },

        /**
         * возраст туриста
         * не обязательное, тип инт, ограничение минимум 0
         * Если возраст указан, он должен быть валидным
         */
        age: { type: Number },
        isAgeValid: {
            type: Boolean,
            computed: ['age', matcher.isAgeValid]
        },

        /**
         * дата рождения туриста
         * 01.06.1980
         * обязательное, тип дата
         * ограничение: максимум = сегодня
         */
        birthday: { type: String },
        /**
         * min - не определён бизнес-требованиями,
         * соответствует initial - maxAge
         */
        minBirthday: {
            type: String,
            computed: ['initial', matcher.minBirthday]
        },
        /** max = today */
        maxBirthday: {
            type: String,
            computed: ['initial', function(initial) { return initial; }]
        },
        isBirthdayValid: {
            type: Boolean,
            computed: ['birthday', 'minBirthday', 'maxBirthday',
                       dates.isDateBetweenInclusive]
        },

        /**
         * фамилия туриста
         * обязательное: латиница
         * ограничение максимум 150 символов
         */
        lastName: { type: String },
        isLastNameValid: {
            type: Boolean,
            computed: ['lastName', matcher.isLatinName]
        },

        /**
         * имя туриста
         * обязательное латиница
         * ограничение максимум 150 символов
         */
        firstName: { type: String },
        isFirstNameValid: {
            type: Boolean,
            computed: ['firstName', matcher.isLatinName]
        },

        /**
         * Достаточно ли данных для возможности вычисления
         */
        isCalculable: {
            type: Boolean,
            computed: ['isAgeValid',
                       matcher.isAnd]
        },

        /**
         * Достаточно ли данных для возможности оплаты
         * Включает в себя isCalculable: хоть и напрямую указывать возраст не требуется для платежа
         */
        isPayable: {
            type: Boolean,
            computed: [
                'isCalculable',
                'isFirstNameValid',
                'isLastNameValid',
                'isBirthdayValid',
                matcher.isAnd
            ]
        }
    };

    provide(model);
});
