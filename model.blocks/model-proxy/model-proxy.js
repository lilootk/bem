modules.define('model-proxy', [
    'policy-instance',
    'request-bus',
    'response-bus',
    'model-view-transformer',
    'functions__debounce'
], function(provide,
            policyInstance,
            requestBus,
            responseBus,
            transformer,
            debounce) {
    // глобальная переменная в текущем модуле
    var stateStorage;

    /**
     * Эта функция выполняется в ответ на специальный пустой request.
     * Такой request должен отправить каждый новый слушатель модели,
     * т.е при старте приложения почти сразу же будет создано множество слушателей
     * и таких одинаковых запросов будет создано очень много практически одновременно,
     * а потом (скорее всего) больше вообще не будет.
     *
     * Чтобы немного разгрузить процесс начальной иниициализации,
     * мы защитим данную функцию c помощью debounсe(),
     * т.к. на все запросы одинаковые начальные запросы
     * response совершенно одинаковый и его одного вполне достаточно на всех.
     */
    var pingResponse = debounce( function() {
        stateStorage.pingState();
    }, 20);

    var sendResponse = function(changedKeys, state) {
        var response = transformer.fixResponse(changedKeys, state);

        console.log('sendResponse',
                    response.changedKeys,
                    response.state);

        responseBus.send(response);
    };

    var handleRequest = function(request) {
        /**
         * особая команда, которую нужно отдельно обрабатывать
         **/
        if (request.cmd === 'ping') {
            pingResponse();
            return;
        }

        if (!request.value) { throw new Error('required_value_in_request'); }

        var prevState = stateStorage.getState();

        switch (request.cmd) {
        case 'set':
            stateStorage.update(transformer.fixUpdate(request.value, prevState));
            return;
        case 'add':
            stateStorage.insertItem
                .apply(stateStorage,
                       transformer.fixInsert(request.key,request.value));
            return;
        case 'remove':
            stateStorage.removeItem
                .apply(stateStorage,
                       transformer.fixRemove(request.key,request.value));
            return;
        default:
            throw 'command_is_not_supported:' + request.cmd;
        }
    };

    var subscribeToSides = function(policyStateStorage) {
        // setTimeout(function() {
        stateStorage = policyStateStorage;
        /**
         * подписываемся на события изменения State
         **/
        stateStorage.subscribe(sendResponse.bind(this));

        /**
         * подписываемся на сообщения из шины request
         **/
        requestBus.subscribe(handleRequest.bind(this));

        /**
         * Модель сообщает всем слушателям созданным раньше чем сама модель,
         * что она родилась
         **/
        pingResponse();
        console.log('A store is created');
        // }, 4000);
        return policyStateStorage;
    };

    policyInstance.init().then(subscribeToSides);

    provide();
});
