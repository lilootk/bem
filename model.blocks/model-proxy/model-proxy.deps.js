({ shouldDeps: [
    'policy-instance',
    'request-bus',
    'response-bus',
    'model-view-transformer',
    { block: 'functions', elem: 'debounce' }
]})
