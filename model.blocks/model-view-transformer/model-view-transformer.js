modules.define('model-view-transformer', [
], function(provide) {
    var responseMatch = {
        'countries': ['country'],
        'tripRange': ['dateStart', 'dateEnd', 'multipolicy']
    };

    var transformer = {};

    transformer.fixResponse = function(changedKeys, state) {
        Object.keys(responseMatch).forEach(function(matchKey) {
            var matchList = responseMatch[matchKey];
            if (changedKeys.indexOf(matchKey) >= 0) {
                changedKeys = changedKeys.concat(matchList);
            }
        });

        state.country = state.countries.map(function(c) {
            return c.id;
        });

        var tripRange = state.tripRange;
        state.dateStart = {
            select: tripRange && tripRange.start,
            min: tripRange && tripRange.minStart,
            max: tripRange && tripRange.maxStart
        };

        state.dateEnd = {
            select: tripRange && tripRange.endOutput,
            min: tripRange && tripRange.minEnd,
            max: tripRange && tripRange.maxEnd,
            disabled: tripRange && tripRange.isFixed
        };

        state.multipolicy = tripRange && tripRange.isFixed;

        return {
            changedKeys: changedKeys,
            state: state
        };
    };

    transformer.fixUpdate = function(changedProps, prevState) {
        var fresh = {};
        if (changedProps.viewUrl !== undefined) {
            fresh['viewUrl'] = changedProps.viewUrl;
        }

        if (changedProps.dateStart !== undefined) {
            fresh['tripRange.start'] = changedProps.dateStart;
        }
        if (changedProps.dateEnd !== undefined) {
            // console.log('dateEnd changed', prevState.tripRange.endFixed, changedProps.dateEnd);
            if (prevState.tripRange.endFixed === changedProps.dateEnd) {
                fresh['tripRange.isFixed'] = true;
            } else {
                fresh['tripRange.end'] = changedProps.dateEnd;
            }
        }
        if (changedProps.multipolicy !== undefined) {
            fresh['tripRange.isFixed'] = changedProps.multipolicy;
            // сброс даты singlePolicy при смене типа полиса
            fresh['tripRange.end'] = null;
        }
        // console.log('transUpdate', changedProps, fresh);
        return fresh;
    };

    transformer.fixInsert = function(key, value) {
        if (key === 'country') {
            return ['countries', { id: value }];
        }
        throw new Error('handle_key:' + key);
    };

    transformer.fixRemove = function(key, value) {
        if (key === 'country') {
            return ['countries', value];
        }
        throw new Error('handle_key:' + key);
    };

    provide(transformer);
});
