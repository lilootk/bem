/**
 * Расширение обычного рэнжа
 * - добавлена возможность фиксации рэнжа: промежуток между начальной и конечной датой становится перманентным (по умолчанию 1 год)
 * - фиксированная дата окончания вычисляется отдельно
 * - итоговая дата окончания вычисляется с учётом флага isFixed
 *
 * Расширение базового типа соответствует принципу Liskov:
 *   Функции, которые используют базовый тип, должны иметь возможность использовать подтипы базового типа, не зная об этом.
 */
modules.define('fixed-date-range-model', [
    'objects',
    'date-range-model',
    'dates',
    'duration-helper'
], function(provide,
            objects,
            dateRangeModel,
            dateHelper,
            durationHelper) {
    var model = {};

    /**
     * Продолжительность по умолчанию: 1 год
     * При необходимости переключать тип полиса: полгода, месяц, год
     *   необходимо внести эту переменную в модель как записываемое поле
     * Данное поле могут изменять соответствующие представления, например
     *   радиогруппа с частовыбираемыми промежутками: P1Y, P6M, P7D
     */
    var FIXED_DURATION = 'P1Y-1D';

    objects.extend(model, dateRangeModel, {
        /**
         * Является ли полис фиксированным
         * alternative: isMultipolicy
         */
        isFixed: {
            type: Boolean
        },

        /**
         * Дата окончания для фиксированного полиса (1 год, полгода)
         * Также используется в обычном полисе в датапикере даты окончания
         */
        endFixed: {
            type: String,
            computed: ['startStamp', function(startStamp) {
                if (startStamp !== null) {
                    return durationHelper.calculateStampPlusDuration(startStamp, FIXED_DURATION);
                }
                return null;
            }]
        },

        /**
         * Итоговое значение даты окончания
         */
        endOutput: {
            type: String,
            computed: ['end', 'endFixed', 'isFixed', function(end, endFixed, isFixed) {
                if (isFixed === true) { return endFixed; }
                if (isFixed === false && end !== null) { return end; }
                return null;
            }]
        },

        /**
         * Валидность выбора дат зависит от типа полиса
         *   и выбранных дат
         */
        isValid: {
            type: Boolean,
            computed: ['isFixed', 'isStartValid', 'isEndValid', function(isFixed, isStartValid, isEndValid) {
                // для фиксированного полиса проверяется только начальная дата
                if (isFixed === true) {
                    return isStartValid;
                }

                if (isFixed === false) {
                    if (isStartValid !== null && isEndValid !== null) {
                        return isStartValid && isEndValid;
                    }
                }

                return null;
            }]
        }
    });

    provide(model);
});
