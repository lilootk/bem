modules.define('policy-state', [
    'computed-state',
    'policy-model'
], function(provide, ComputedState, policyModel) {
    var PolicyState = function() {
        ComputedState.call(this, policyModel);
    };

    PolicyState.prototype = Object.create(ComputedState.prototype);

    provide(PolicyState);
});
