modules.define('spec', [
    'chai',
    'policy-state'
], function(provide, chai, PolicyState) {
    var expect = chai.expect;

    // Демо данные: выборка из реального источника данных
    // Экземпляр не модефицируем, используется между тестами
    var originCountriesData = [
        {"id":"g_schengen","group":"group","name":"Все страны Шенгена","search":"Все страны Шенгена Dct cnhfys Ityutyf"},
        {"id":"g_all-world","group":"group","name":"Весь мир","search":"Весь мир Dtcm vbh"},
        {"id":"g_world-no-russia","group":"group","name":"Весь мир, кроме России","search":"Весь мир, кроме России Dtcm vbh, rhjvt Hjccbb"},
        {"id":"g_south-asia","group":"group","name":"Юго-Восточная Азия","search":"Юго-Восточная Азия >uj-Djcnjxyfz Fpbz"},
        {"id":"france","group":"country","name":"Франция","search":"Франция Французская Республика France FRANCE, SHENGEN Ahfywbz Ahfywepcrfz Htcge,kbrf"},
        {"id":"finland","group":"country","name":"Финляндия","search":"Финляндия Финляндская Республика Finland FINLAND, SHENGEN Шенген (получение визы в Финляндию) Abykzylbz Abykzylcrfz Htcge,kbrf"},
        {"id":"estonia","group":"country","name":"Эстония","search":"Эстония Эстонская Республика Estonia ESTONIA, SHENGEN \"cnjybz \"cnjycrfz Htcge,kbrf"},
    ];

    describe('model-proxy', function() {
        var store;
        beforeEach(function() {
            store = new PolicyState();

            // создание нужного кол-ва нулевых туристов
            // подобно нулевому insurer

            // var tourists = new Array(4).map(function(item) {
            //     return null;
            // });

            // Значения по умолчанию
            store.update({
                tripRange: {
                    // сегодня
                    initial: '01.01.2016',
                    // 1 год минус 1 день
                    maxDuration: 'P1Y-1D',
                    // обычный негодовой полис
                    isFixed: false
                }
                //tourists: tourists
            });
        });

        afterEach(function() {
            store = null;
        });

        it('should ready', function(done) {
            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'dateVisa',
                    'dateVisaStamp',
                    'tripRange',
                    'isDateVisaValid'
                ]);

                expect(state.dateVisa).to.equal('03.04.2009');
                expect(state.dateVisaStamp).to.equal(1238716800000);
                expect(state.tripRange.start).to.equal('01.05.2010');
                expect(state.tripRange.startStamp).to.equal(1272672000000);
                // пока страна не выбрана - null
                expect(state.policyDateStart).to.null;
                expect(state.policyDateEnd).to.null;
                done();
            });

            store.update({
                dateVisa: '03.04.2009',
                tripRange: {
                    initial: '05.04.2010',
                    start: '01.05.2010',
                    maxDuration: 'P1Y'
                }
            });
        });

        it('should update originCountries', function(done) {
            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'originCountries'
                ]);

                expect(state.originCountries.length).to.equal(7);

                var finland = state.originCountries[5];
                expect(finland.isGroup).to.false;
                expect(finland.isDateVisaRequired).to.true;

                expect(state.isDateVisaRequired).to.null;
                done();
            });

            store.update({
                originCountries: originCountriesData
            });
        });

        it('should insertItem', function(done) {
            store.update({
                originCountries: originCountriesData
            });

            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'countries',
                    'isCountriesEnough',
                    'isDateVisaRequired'
                ]);

                expect(state.isDateVisaRequired).to.true;
                expect(state.isDateVisaValidIfRequired).to.null;
                expect(state.isCalculable).to.null;
                done();
            });

            store.insertItem('countries', { id: 'estonia' });
        });

        it('should insertItem country + visa', function(done) {
            store.update({
                originCountries: originCountriesData
            });

            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'countries',
                    'isCountriesEnough',
                    'isDateVisaRequired',
                    'tripRange',
                    'policyDateEnd',
                    'dateVisa',
                    'dateVisaStamp',
                    'isDateVisaValid',
                    'isDateVisaValidIfRequired',
                    // 'isCalculable',
                    // 'isPayable',
                    'policyDateStart'
                ]);

                expect(state.isDateVisaRequired).to.true;
                expect(state.isDateVisaValid).to.true;
                expect(state.isDateVisaValidIfRequired).to.true;
                expect(state.isCountriesEnough).to.true;
                expect(state.isTouristsCalculable).to.null;
                // Не хватает данных по туристам
                expect(state.isCalculable).to.null;
                // Не хватает данных по туристам
                expect(state.isPayable).to.null;
                expect(state.policyDateStart).to.equal('02.02.2016');
                expect(state.policyDateEnd).to.equal('15.02.2016');
                done();
            });

            store.update({
                countries: [{ id: 'estonia' }],
                'tripRange.start': '05.02.2016',
                'tripRange.end': '15.02.2016',
                dateVisa: '02.02.2016'
                // tourists: []
            });
        });

        it('should removeItem', function(done) {
            store.update({
                originCountries: originCountriesData
            });

            store.insertItem('countries', {
                id: 'estonia'
            });

            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'countries',
                    'isCountriesEnough',
                    'isDateVisaRequired'
                ]);

                expect(state.isCalculable).to.null;
                done();
            });

            store.removeItem('countries', 'estonia');
        });

        it('should update subobject', function(done) {
            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'insurer'
                ]);

                expect(state.insurer).to.not.undefined;

                expect(state.insurer.name).to.equal('zzz');

                done();
            });

            store.update({
                insurer: { name: 'zzz', lastName: 'qqq' }
            });
        });

        it('should update nonexisting subproperty', function() {
            var change = store.update.bind(store, {
                'insurer.name': 'qqq',
                'insurer.lastName': 'zzz'
            });

            expect(change).to.throw('no_such_property_to_set');
        });

        it('should update existing subproperty', function(done) {
            store.update({
                insurer: {
                    initial: '31.12.2016'
                }
            });

            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'insurer'
                ]);

                var insurer = state.insurer;

                expect(insurer.isLastNameValid).to.false;
                expect(insurer.isNameValid).to.true;
                expect(insurer.birthdayMin).to.equal('31.12.1896');
                // 18 years at least
                expect(insurer.birthdayMax).to.equal('31.12.1998');
                expect(insurer.isBirthdayValid).to.true;
                done();
            });

            store.update({
                'insurer.name': 'John',
                'insurer.lastName': 'фыва',
                'insurer.birthday': '05.05.1998'
            });
        });

        it('should insert a tourist', function(done) {
            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'tourists',
                    'isTouristsCalculable',
                    'isTouristsPayable'
                ]);

                var tourist = state.tourists[0];

                expect(tourist.lastName).to.equal('asdf');
                expect(tourist.isLastNameValid).to.true;
                expect(tourist.isAgeValid).to.true;
                expect(tourist.isCalculable).to.true;
                expect(tourist.isPayable).to.null;
                done();
            });

            store.insertItem('tourists', {
                id: 1,
                firstName: 'asdf',
                lastName: 'asdf',
                initial: '01.01.2016',
                age: 99
            });
        });

        // works in a new version
        // it('should update a tourist', function(done) {
        //     store.insertItem('tourists', {
        //         id: 5,
        //         firstName: 'asdf',
        //         lastName: 'asdf',
        //         initial: '01.01.2016',
        //         age: 99
        //     });

        //     store.subscribe(function(changedKeys, state) {
        //         expect(changedKeys).to.deep.equal([
        //             'tourists'
        //         ]);

        //         var tourist = state.tourists.filter(function(t) {
        //             return t.id === 5;
        //         })[0];

        //         expect(tourist).to.not.undefined;
        //         expect(tourist.lastName).to.equal('Lancaster');
        //         expect(tourist.age).to.equal(25);
        //         expect(tourist.isLastNameValid).to.true;
        //         expect(tourist.isAgeValid).to.true;
        //         expect(tourist.isCalculable).to.true;
        //         expect(tourist.isPayable).to.null;
        //         done();
        //     });

        //     store.update({
        //         'tourists.5.lastName': 'Lancaster',
        //         'tourists.5.age': 25
        //     });
        // });

        it('should remove a tourist', function(done) {
            store.insertItem('tourists', {
                id: 5,
                firstName: 'asdf',
                lastName: 'asdf',
                initial: '01.01.2016',
                age: 99
            });

            store.subscribe(function(changedKeys, state) {
                expect(changedKeys).to.deep.equal([
                    'tourists',
                    'isTouristsCalculable'
                ]);

                expect(state.tourists).to.deep.equal([]);
                expect(state.isTouristsCalculable).to.false;
                done();
            });

            store.removeItem('tourists', 5);
        });
    });

    provide();
});
