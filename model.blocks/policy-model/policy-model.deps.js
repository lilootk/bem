[{
    shouldDeps: [
        'dates',
        'matcher',
        'fixed-date-range-model',
        'insurer-model',
        'tourist-model',
        'country-model',
        'origin-country-model'
    ]
}];
