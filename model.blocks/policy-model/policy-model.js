/**
 * Страховой полис
 * Все даты находятся в формате ГОСТ: dd.mm.yyyy
 * Итоговый срок полиса будет выводится только после выбора компании
 * Не все компании прибавляют 15 дней для годового полиса в страны Шенгена
 */
modules.define('policy-model', [
    'dates',
    'matcher',
    'fixed-date-range-model',
    'tourist-model',
    'insurer-model',
    'country-model',
    'origin-country-model'
], function(provide,
            dates,
            matcher,
            fixedDateRangeModel,
            touristModel,
            insurerModel,
            countryModel,
            originCountryModel) {
    var model = {
        /**
         * Url, где находится пользователь на данный момент
         * по отношению к полису данное поле можно рассматривать как
         *   прогресс-состояние, в котором находится заполнение полиса
         */
        viewUrl: { type: String },

        /**
         * Дата начала, окончания поездки в зависимости от типа полиса:
         * фиксированного или обычного
         */
        tripRange: { type: fixedDateRangeModel },

        /**
         * Ориентировочная дата подачи документов на визу в визовом центре
         * Требуется указать для некоторых стран, например для Финляндии
         * Пример:
         * человек выставляет дату начала предположим 28.10, дату конца 30.10, а дату визы 15.10 - получится, что полис будет действовать с 15.10 по 14.11 (еще и +15 дней для шенгена), но всего на 3 застрахованных дней
         * если при этом выбрать годовой полис, то дата окончания получится 27.10.2017  и соответсвенно полис будет длительностью больше года: с 15.10.2016 до 27.10.2017. И помимо этого приплюсовывать ещё 15 дней (для Шенгена)
         */
        dateVisa: { type: String },

        /**
         * Кол-во дней страхования
         * По сути это может быть любое значение в пределах разницы дат
         * Наиболее вероятные значения определяются в разметке
         * Количество дней страхования, которые будут расходоваться в течение года при каждой поездке.
         * insuredDays <= (dateEnd - dateStart)
         * Вы можете дробить свои поездки, например:
         *  5 дней + 10 дней + 5 дней
         * пока не израсходуете все дни страхования.
         * Возможные значения
         * 30, 45, 60, 90, 180, 365
         * @TODO проверять возможные значения
         * @type {Number}
         */
        insuredDays: { type: Number },

        /**
         * Данные о застрахованных лицах (туристах)
         */
        tourists: { type: [touristModel] },
        /**
         * Страхователь, лицо оформляющее страховой полис
         */
        insurer: { type: insurerModel },

        /** Список стран (идентификаторов), выбранных пользователем */
        countries: { type: [countryModel] },

        /** Список стран, доступный для выбора пользователю */
        originCountries: { type: [originCountryModel] },

        /** Достаточно ли выбрано стран для калькуляции и оформления полиса */
        isCountriesEnough: {
            type: Boolean,
            computed: ['countries', function(countries) {
                if (!countries) { return null; }
                return countries.length >= 1;
            }]
        },

        /** Дата визы в формате UTC timestamp */
        dateVisaStamp: {
            type: Number,
            computed: ['dateVisa', dates.fromGostToStamp]
        },

        /**
         * Дата визы должна быть:
         * - больше+равно текущей даты
         * - и раньше+равно даты начала поездки
         */
        isDateVisaValid: {
            type: Boolean,
            computed: [
                'dateVisaStamp', 'tripRange',
                function(dateVisaStamp, tripRange) {
                    if (dateVisaStamp !== null &&
                        tripRange !== null &&
                        tripRange.initialStamp !== null &&
                        tripRange.startStamp !== null) {
                        return dateVisaStamp >= tripRange.initialStamp &&
                        dateVisaStamp <= tripRange.startStamp;
                    }

                    return null;
                }]
        },

        /**
         * Для некоторых стран необходимо указывать дату визы.
         * Если требуется, необходимо отобразить датапикер с датой визы
         * Проверяются все выбранные страны на предмет необходимости в наличие даты визы
         */
        isDateVisaRequired: {
            type: Boolean,
            computed: ['countries', 'originCountries', function(countries, originCountries) {
                // если страны не подгружены и ещё не заполнены
                //  тогда результат неактуален: нельзя сказать, требуется ли дата визы, если ещё не выбрана ни одна страна
                if (countries.length === 0 ||
                    originCountries.length === 0) { return null; }

                // входит ли оригин страна в список выбранных стран
                var checkOriginCountry = function(originCountry) {
                    if (originCountry.isDateVisaRequired !== true) {
                        return false;
                    }

                    return countries.some(function(country) {
                        return originCountry.id === country.id;
                    });
                };

                return originCountries.some(checkOriginCountry);
            }]
        },

        /**
         * Валидна ли указанная дата визы в случае если она требуется.
         * Для многих стран дата визы не требуется, и валидность введённой даты не имеет значения.
         * В этом случае необходимо обнулять дату визы и скрывать соответствующий датапикер.
         */
        isDateVisaValidIfRequired: {
            type: Boolean,
            computed: [
                'isDateVisaRequired', 'isDateVisaValid',
                function(isDateVisaRequired, isDateVisaValid) {
                    if (isDateVisaRequired === false) { return true; }
                    if (isDateVisaRequired === true) {
                        return isDateVisaValid; // true, false, null
                    }
                    return null;
                }]
        },

        /**
         * Достаточно ли данных по туристам для калькуляции
         */
        isTouristsCalculable: {
            type: Boolean,
            computed: ['tourists', function(tourists) {
                var calculableTourists = tourists.filter(function(t) {
                    return t && t.isCalculable === true;
                });
                return calculableTourists.length >= 1;
            }]
        },

        /**
         * Достаточно ли данных по туристам для возможсти оплаты
         */
        isTouristsPayable: {
            type: Boolean,
            computed: ['tourists', function(tourists) {
                var payableTourists = tourists.filter(function(t) {
                    return t && t.isPayable === true;
                });
                return payableTourists.length >= 1;
            }]
        },

        /**
         * Правильно ли заполнены все данные необходимые для калькуляции?
         * Кнопка "Рассчитать" активируется, если всё валидно.
         */
        isCalculable: {
            type: Boolean,
            computed: [
                'isDateVisaValidIfRequired',
                'isCountriesEnough',
                'isTouristsCalculable',
                matcher.isAnd
            ]
        },

        /**
         * Правильно ли заполнены все данные для возможности покупки.
         */
        isPayable: {
            type: Boolean,
            computed: [
                'isCalculable',
                'isTouristsPayable',
                matcher.isAnd
            ]
        },

        policyDateStart: {
            type: String,
            computed: [
                'tripRange', 'dateVisa', 'isDateVisaRequired',
                function(tripRange, dateVisa, isDateVisaRequired) {
                    if (isDateVisaRequired === true) {
                        return dateVisa;
                    }

                    if (isDateVisaRequired === false && tripRange !== null) {
                        return tripRange.start;
                    }

                    return null;
                }]
        },

        /**
         * TODO: зависит от выбранной компании
         * Дата окончания полиса вычисляется только после рассчёта и выбора компании, так как не все компании добавляют 15 дней для Шенгена (годовой полис)
         */
        policyDateEnd: {
            type: String,
            computed: [
                'tripRange',
                function(tripRange) {
                    if (tripRange !== null) {
                        return tripRange.endOutput;
                    }

                    return null;
                }]
        }
    };

    provide(model);
});


// http://docs.cherehapa.ru/api/docs
// Ограничения на значения парметров для всех запросов:insurer[name], insurer[lastName] - не более 50 символов, допустимые символы согласно регулярному выражению: [a-Z- ]+ insurer[nameRU] , insurer[nameRU] и insurer[nameRU] до 65535 символов insurer[birthday] - дата, такая чтобы возраст был от 18 до 120 лет insurer[email] до 50 символов insurer[phone] до 255 символов страна обязательна при не указанной группе стран значение в tourist[age] от 0 currency должно иметь все символы либо в верхнем либо в нижнем регистре, не может быть RUB marker до 255 символов card[year] - два символа, card[cvc] - три символа при создании полиса dateStart, dateEnd и dateVisa не ранее текущего дня, поля периода страхования взаимосвязаны, insuredDays не может превышать разности дат dateStart и dateEnd, а для страховых не поддерживающих это поле должен быть строго равен этой разности, dateEnd - не может быть раньше dateStart и позже dateStart + 1 год
