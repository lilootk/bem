({
    title: 'Cherehapa страхование. Туристическая страховка онлайн',
    head: [
        { elem: 'meta', attrs: { name: 'viewport', content: 'width=device-width, initial-scale=1' } },
        { elem: 'css', url: 'index.min.css' }
    ],
    scripts: [{ elem: 'js', url: 'index.min.js' }],

    block: 'page',
    mix: [
        { block: 'main', js: true },
        { block: 'main-menu', js: true },
        { block: 'init-models' }
    ],
    content: [
        {
            block: 'header'
        },
        {
            block: 'main-menu',
            elem: 'content',
            content: [
                { url: '#home', content: 'В начало' },
                { url: '#countries', content: 'Выбор стран' },
                { url: '#insurable', content: 'Что и зачем страховать?' },
                { url: '#about', content: 'О компании' },
                { url: '#contacts', content: 'Контакты' },
                { url: '#faq', content: 'Вопросы и ответы' },
            ]
        },
        {
            block: 'view',
            url: '#home',
            content: [
                {
                    block: 'countries-selected'
                },
                {
                    block: 'countries-select-button',
                    url: '#countries',
                    any: 'Добавить страну',
                    none: 'Выбрать страну'
                },
                {
                    block: 'wrapper-twin',
                    content: [
                        {
                            mix: {block: 'wrapper-twin', elem: 'left'},
                            block: 'input-date',
                            prop: 'dateStart',
                            label: 'Дата начала'
                        },
                        {
                            mix: {block: 'wrapper-twin', elem: 'right'},
                            block: 'input-date',
                            prop: 'dateEnd',
                            label: 'Дата окончания'
                        }
                    ]
                },
                {
                    block: 'input-switch',
                    prop: 'multipolicy',
                    label: 'Нужен годовой полис'
                },
                {
                    block: 'view-button',
                    url: '#travelers',
                    content: 'Данные путешественников'
                },
                { block: 'footer' }
            ]
        },
        {
            block: 'view',
            url: '#countries',
            title: 'Выберите страны поездки',
            content: [
                { block: 'countries-selected' },
                {
                    block: 'view-button',
                    mods: { 'history-back': true },
                    url: '#home',
                    content: 'OK'
                },
                { block: 'countries-select' }
            ]
        },
        {
            block: 'view',
            url: '#travelers',
            title: 'Данные о путешественниках',
            content: [
                { block: 'view-travelers-content' },
                {
                    block: 'view-button',
                    mods: { 'history-back': true },
                    url: '#home',
                    content: 'OK'
                }
            ]
        },
        {
            block: 'view',
            url: '#about',
            title: 'О компании Cherehapa',
            theme: 'blue',
            content: { block: 'view-about-content' }
        },
        {
            block: 'view',
            url: '#faq',
            title: 'Вопросы и ответы страхования в Cherehapa',
            theme: 'blue',
            content: { block: 'view-faq-content' }
        },
        {
            block: 'view',
            url: '#contacts',
            title: 'Контакты Cherehapa',
            theme: 'blue',
            content: { block: 'view-contacts-content' }
        },
        {
            block: 'view',
            url: '#insurable',
            title: 'Что и зачем страховать?',
            theme: 'blue',
            content: { block: 'view-insurable-content' }
        },
        {
            block: 'view',
            title: 'Cherehapa: ошибка 404',
            theme: 'blue',
            content: { block: 'view-404-content' }
        }

    ]
})
