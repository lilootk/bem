modules.define('arrays',
function (provide) { provide({

    clone: function (array) {
        return [].concat(array);
    },

    findIndex: function (array, predicate, thisArg) {
        for (var i = 0, length = array.length; i < length; ++i) {
            if (predicate.call(thisArg, array[i], i, array)) {
                return i;
            }
        }
        return -1;
    },

    find: function (array, predicate, thisArg) {
        var i = this.findIndex.apply(this, arguments);
        return (i === -1) ? undefined : array[i];
    },

    indexOf: function (array, value, key) {
        if (!key) {
            return array.indexOf(value);
        }

        var predicate = function(element) {
            return element[key] === value;
        }

        return this.findIndex(array, predicate);
    },

    diff: function (array, diff) {
        if (!diff.length) {
            return array;
        }

        var predicate = function(value) {
            return diff.indexOf(value) === -1;
        };

        return array.filter(predicate);
    },

    only: function (array, only) {
        if (!only.length) {
            return [];
        }

        var predicate = function(value) {
            return only.indexOf(value) !== -1;
        };

        return array.filter(predicate);
    },

    merge: function (array, merge) {
        var result = this.clone(array);

        var pushUnique = function(value) {
            if (result.indexOf(value) === -1) {
                result.push(value);
            }
        };

        merge.forEach(pushUnique);

        return result;
    },

    pushUnique: function (array, value) {
        return this.merge(array, [value]);
    },

    remove: function (array, value) {
        return this.diff(array, [value]);
    }

});}
);
