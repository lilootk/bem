modules.define('strings__soft-compare', function(provide) {

//русский алфавит фонетически или графически соответствующими латинскими буквами и их сочетаниями
var translit = {
    a:'а', b:'б', c:'ц', d:'д', e:'е', f:'ф', g:'г', h:'х', i:'и', j:'й',
    k:'к', l:'л', m:'м', n:'н', o:'о', p:'п', q:'ц', r:'р', s:'с', t:'т',
    u:'у', v:'в', w:'ш', x:'h', y:'ы', z:'з',
    jo:'е', yo:'е', ju:'ю', yu:'ю', iu:'ю', ia:'я', ja:'я', ya:'я', je:'э',
    zh:'ж', ch:'ч', sh:'ш', kh:'х', tc:'ц', ts:'ц', shch:'щ',
    "'":'ь', '"':'ь'
};

//русские буквы на клавиатуре в английском регистре ввода
var keymap = {
    '~':'е', '`':'е', 'ё':'е',
    q:'й', w:'ц', e:'у', r:'к', t:'е', y:'н', u:'г', i:'ш', o:'щ', p:'з', '[':'х', '{':'х', ']':'ъ', '}':'ъ',
    a:'ф', s:'ы', d:'в', f:'а', g:'п', h:'р', j:'о', k:'л', l:'д', ':':'ж', ';':'ж', '"':'э',  "'":'э',
    z:'я', x:'ч', c:'с', v:'м', b:'и', n:'т', m:'ь', ',':'б', '<':'б', '.':'ю','>':'ю'
};

var makeIndex = function(str) {
    return str.split('|').map(makeSoft);
};

var makeSoft = function(str) {
    var s = str.trim().toLowerCase();

    var result = [];
    for (var i = 0; i < s.length; ++i) {
        var ch = s[i];
        result.push(keymap[ch] ? keymap[ch] : ch);
    }
    return result.join();
};

var softCompare = function(softIndex, softNeedle) {
    return softIndex.some(function(str) {
        return softNeedle == str.substr(0, softNeedle.length);
    });
};

provide({
    makeIndex: makeIndex,
    makeSoft: makeSoft,
    softCompare: softCompare
});

});
