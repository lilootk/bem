modules.define('dates', function (provide) {
    var Dates = {};

    Dates.defaultFormat = function () {
        return 'dd.mm.yyyy';
    };

    Dates.formatDate = function (dateable, toISO) {
        var date = Dates.parseDate(dateable);
        if (!date) {
            throw 'Не задана дата';
        }

        var yyyy = date.getUTCFullYear();
        var mm = date.getUTCMonth() + 1; //January is 0!
        var dd = date.getUTCDate();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        return toISO
            ? yyyy + '-' + mm + '-' + dd
            : dd + '.' + mm + '.' + yyyy;
    };

    Dates.parseDate = function (dateable) {
        if (dateable == null) {
            throw 'Не задана дата';
        }

        var date = Dates.isInteger(dateable) ? new Date(dateable) : dateable;

        if (date instanceof Date) {
            //нормализуем дату
            return new Date(Date.UTC(
                date.getUTCFullYear(),
                date.getUTCMonth(),
                date.getUTCDate()
            ));
        }

        //pickadate format
        if (typeof date === 'object' &&
            Dates.isInteger(date.year) &&
            Dates.isInteger(date.month) &&
            Dates.isInteger(date.date)) {

            return new Date(Date.UTC(
                date.year,
                date.month,
                date.date
            ));
        }

        var DMY = /\d{1,2}\.\d{1,2}\.\d{2,4}/;
        var ISO = /\d{2,4}-\d{1,2}-\d{1,2}/;
        var MDY = /\d{1,2}\/\d{1,2}\/\d{2,4}/;
        var yyyy, mm, dd;

        if (DMY.test(date)) {
            var dateSplit = date.split('.');
            dd = dateSplit[0];
            mm = dateSplit[1];
            yyyy = dateSplit[2];
        }
        else if (ISO.test(date)) {
            var dateSplit = date.split('-');
            yyyy = dateSplit[0];
            mm = dateSplit[1];
            dd = dateSplit[2];
        }
        else if (MDY.test(date)) {
            var dateSplit = date.split('/');
            mm = dateSplit[0];
            dd = dateSplit[1];
            yyyy = dateSplit[2];
        }
        else {
            var timestamp = Date.parse(date);

            if (isNaN(timestamp)) {
                console.error('Неизвестный формат даты', date);
                throw 'Неизвестный формат даты';
            }

            var newDate = new Date(timestamp);

            return new Date(Date.UTC(
                newDate.getUTCFullYear(),
                newDate.getUTCMonth(),
                newDate.getUTCDate()
            ));
        }

        if (31 < yyyy && yyyy < 100) {
            yyyy = 1900 + +yyyy;
        }

        return new Date(Date.UTC(yyyy, mm - 1, dd));
    };

    // '31.12.1999' -> 1234560000
    Dates.fromGostToStamp = function(gost) {
        if (!gost) { return null; }

        var parts = gost.split('.');
        var dd = parseInt(parts[0], 10);
        var mm = parseInt(parts[1], 10) - 1;
        var yyyy = parseInt(parts[2], 10);

        return Date.UTC(yyyy, mm, dd);
    };

    // new Date() -> '01.01.2016'
    Dates.fromDateToGost = function(date) {
        if (!date) { return null; }

        var dd = date.getUTCDate();
        var mm = date.getUTCMonth() + 1;
        var yyyy = date.getUTCFullYear();

        return [
            dd < 10 ? ('0' + dd) : dd,
            mm < 10 ? ('0' + mm) : mm,
            yyyy
        ].join('.');
    };

    // 1451595600000 -> '01.01.2016'
    Dates.fromStampToGost = function(stamp) {
        if (stamp === null || stamp === undefined) { return null; }
        return Dates.fromDateToGost(new Date(stamp));
    };

    Dates.isDateBetweenInclusive = function(date, minDate, maxDate) {
        if (date === null || minDate === null || maxDate === null) {
            return null;
        }

        var dateStamp = Dates.fromGostToStamp(date);
        var minDateStamp = Dates.fromGostToStamp(minDate);
        var maxDateStamp = Dates.fromGostToStamp(maxDate);

        return dateStamp >= minDateStamp && dateStamp <= maxDateStamp;
    };

    Dates.diffDaysBetweenStamps = function(stampEnd, stampStart) {
        if (stampEnd === null || stampStart === null) { return null; }
        var msecsPerDay = 24 * 60 * 60 * 1000;
        var days = (stampEnd - stampStart) / msecsPerDay;

        return Math.round(days);
    };

    //возвращает разницу между датами в днях
    Dates.diffDays = function (dateableEnd, dateableStart) {
        var dateEnd = Dates.parseDate(dateableEnd);
        var dateStart = Dates.parseDate(dateableStart);
        return Dates.diffDaysBetweenStamps(dateEnd.getTime(), dateStart.getTime());
    };

    //возвращает разницу между датами в годах
    Dates.diffYears = function (dateableEnd, dateableStart) {
        var dateEnd = Dates.parseDate(dateableEnd);
        var dateStart = Dates.parseDate(dateableStart);

        var years = dateEnd.getUTCFullYear() - dateStart.getUTCFullYear();

        var monthDiff = dateEnd.getUTCMonth() - dateStart.getUTCMonth();
        var daysDiff = dateEnd.getUTCDate() - dateStart.getUTCDate;
        if (monthDiff < 0 || (monthDiff == 0 && daysDiff < 0)) {
            return years - 1;
        }

        return years;
    };

    Dates.toInteger = function (n) {
        return n >= 0 ? Math.floor(n) : Math.ceil(n);
    };

    Dates.isInteger = function (n) {
        return (typeof n === 'number') &&
            isFinite(n) &&
            (Dates.toInteger(n) === n);
    };

    Dates.today = function () {
        var now = new Date();
        return Dates.moscowDate(now);
    };

    /**
     * @param {Date} date Any JS date
     * @param {Number} offset https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date/getTimezoneOffset
     * @returns {String} Formatted date
     */
    Dates.offsetDate = function(date, offset) {
        var localTime = date.getTime();
        var offsetMilliseconds = offset * 60 * 1000;
        var resultDate = new Date(localTime - offsetMilliseconds);
        return Dates.formatDate(resultDate);
    };

    Dates.moscowDate = function (date) {
        return Dates.offsetDate(date, -180);
    };

    Dates.plusDays = function(dateable, days) {
        var date = Dates.parseDate(dateable);
        date.setUTCDate(date.getUTCDate() + days);
        return Dates.formatDate(date);
    };

    Dates.plusYear = function (dateable) {
        var date = Dates.parseDate(dateable);
        date.setUTCFullYear(date.getUTCFullYear() + 1);
        return Dates.formatDate(date);
    };

    provide(Dates);
});
