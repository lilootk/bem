modules.define('spec', ['dates', 'chai'],
function (provide, Dates, chai) {
    var assert = chai.assert;

    describe('dates', function() {

        it('isInteger', function() {
            [0, 1, -1, 100500, -100500, 1e6, 100.0]
            .forEach( function(n) {
                assert.equal(Dates.isInteger(n), true, 'isInteger: ' + n);
            });

            [
                null, undefined, true, false,
                '', '0', '1', '01.01.1970', 'Черехапа',
                0.5, -0.001, NaN, -NaN, Infinity, -Infinity,
                new Date(), {}, [], [0], [0, 1], { val: 1 }
            ]
            .forEach( function(n) {
                assert.equal(Dates.isInteger(n), false, 'notInteger: ' + n);
            });
        });

        it('formatDate', function() {
            assert.equal(Dates.formatDate('11.09.2001'), '11.09.2001', 'date equal to date');
            assert.equal(Dates.formatDate(0), '01.01.1970', 'start of epoch');
            assert.equal(Dates.formatDate('1.2.34'), '01.02.1934', 'short year');
            assert.equal(Dates.formatDate('9/11/2001'), '11.09.2001', 'american year');
        });

        it('should fromGostToStamp', function() {
            assert.equal(Dates.fromGostToStamp('31.12.1999'), 946598400000);
        });

        it('should fromStampToGost', function() {
            assert.equal(Dates.fromStampToGost(946598400000), '31.12.1999');
        });

        it('should fromStampToGost', function() {
            assert.equal(Dates.fromStampToGost(1299196800000), '04.03.2011');
        });

        it('should isDateBetweenInclusive', function() {
            var d = '01.01.2010';
            assert.equal(Dates.isDateBetweenInclusive(d, null, null), null);
            assert.equal(Dates.isDateBetweenInclusive(d, '02.01.2009', '04.03.2010'), true);
            assert.equal(Dates.isDateBetweenInclusive(d, '02.01.2010', '04.03.2010'), false);
        });

        it('diffDays', function() {
            assert.equal(Dates.diffDays('07 Nov 2016', '07 Nov 2016'), 0, 'same dates diff');
            assert.equal(Dates.diffDays('07 Nov 2016', '06 Nov 2016'), 1, 'date equal to date');
            assert.equal(Dates.diffDays('06 Nov 2016', '07 Nov 2016'), -1, 'date equal to date');
            assert.equal(Dates.diffDays('07 Nov 2016', '07 Nov 2015'), 366, 'leap year diff');
            assert.equal(Dates.diffDays('07 Nov 2016', '07 Nov 2017'), -365, 'common year diff');
        });

        it('diffYears', function() {
            assert.equal(Dates.diffYears('07 Nov 2016', '07 Nov 2016'), 0, 'same dates diff');
            assert.equal(Dates.diffYears('07 Nov 2017', '07 Nov 2016'), +1, 'full year');
            assert.equal(Dates.diffYears('07 Nov 2016', '07 Nov 2017'), -1, 'full year');
            assert.equal(Dates.diffYears('2016-02-29', '2015-02-28'), +1, 'leap year diff');
            assert.equal(Dates.diffYears('2016-02-29', '2017-02-28'), -1, 'leap year diff');
            assert.equal(Dates.diffYears('2016-02-28', '2015-02-28'), 1, 'leap year diff');
            assert.equal(Dates.diffYears('2016-02-28', '2017-02-28'), -1, 'leap year diff');
        });

        it('plusYear', function() {
            assert.equal(Dates.plusYear(0), '01.01.1971', 'happy new year');
            assert.equal(Dates.plusYear('29.02.1972'), '01.03.1973', 'leap year');
        });

        it('offsetDate', function() {
            var demoOffset = -120;
            assert.equal(Dates.offsetDate(new Date('07 Nov 2016  0:00 UTC+0300'), demoOffset), '06.11.2016', 'Moscow time');

            assert.equal(Dates.offsetDate(new Date('07 Nov 2016 23:59 UTC+0300'), demoOffset), '07.11.2016', 'Moscow time');

            assert.equal(Dates.offsetDate(new Date('07 Nov 2016  0:00 UTC-0500'), demoOffset), '07.11.2016', 'New-York time');
            assert.equal(Dates.offsetDate(new Date('07 Nov 2016 23:59 UTC-0500'), demoOffset), '08.11.2016', 'New-York time');

            assert.equal(Dates.offsetDate(new Date('07 Nov 2016  0:00 UTC+0700'), demoOffset), '06.11.2016', 'Novosibirsk time');
            assert.equal(Dates.offsetDate(new Date('07 Nov 2016 23:59 UTC+0700'), demoOffset), '07.11.2016', 'Novosibirsk time');
        });

        it('moscowDate', function() {
            assert.equal(Dates.moscowDate(new Date('07 Nov 2016  0:00 UTC+0300')), '07.11.2016', 'Moscow time');
            assert.equal(Dates.moscowDate(new Date('07 Nov 2016 23:59 UTC+0300')), '07.11.2016', 'Moscow time');

            assert.equal(Dates.moscowDate(new Date('07 Nov 2016  0:00 UTC-0500')), '07.11.2016', 'New-York time');
            assert.equal(Dates.moscowDate(new Date('07 Nov 2016 23:59 UTC-0500')), '08.11.2016', 'New-York time');

            assert.equal(Dates.moscowDate(new Date('07 Nov 2016  0:00 UTC+0700')), '06.11.2016', 'Novosibirsk time');
            assert.equal(Dates.moscowDate(new Date('07 Nov 2016 23:59 UTC+0700')), '07.11.2016', 'Novosibirsk time');
        });

    });

    provide();

});
