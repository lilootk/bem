modules.define('objects',
function(provide, Objects) {

    var clone = function(object) {
        try {
            return (object === undefined) ? undefined : JSON.parse(JSON.stringify(object));
        }
        catch (e) {
            console.warn('игнорируем exception внутри Objects.clone()');
            return undefined;
        }
    };

    var only = function(object, keys) {
        var result = {};

        keys.forEach( function(key) {
            if (object[key] != null) {
                result[key] = clone(object[key]);
            }
        });

        return result;
    };

    provide(Objects.extend(Objects, { clone: clone, only: only }));

}
);
