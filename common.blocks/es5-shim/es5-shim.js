/**
 * Поддержка es5 старыми браузерами
 * https://github.com/es-shims/es5-shim
 *
 * - phantomjs (запуск юнит-тестов) не поддерживает некоторые es5 возможности, например .bind()
 * - BEMHTML использует .bind(): TypeError: 'undefined' is not a function (evaluating 'this.body.bind(this)')
 *
 * По методологии - надо подключать в каждый блок, где используется es5
 * - Array.prototype.filter
 * - Array.prototype.join
 * - Function.prototype.bind
 * - String.prototype.split
 * - и так далее
 */
modules.define('es5-shim', function(provide) {
    // этот комментарий будет заменен `borschik`-ом на содержимое файла плагина
    /*borschik:include:../../libs/es5-shim/es5-shim.js*/

    provide();
});
