/**
 * Глобальная очередь команд из view в models
 */
modules.define('request-bus', ['message-bus'],

    function (provide, MessageBus) {
        provide(new MessageBus('requestBus'));
    }

);
