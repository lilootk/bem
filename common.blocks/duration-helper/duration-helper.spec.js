modules.define('spec', [
    'duration-helper',
    'chai'
], function (provide, helper, chai) {
    var assert = chai.assert;

    describe('duration-helper', function() {

        it('should calculateStampPlusDuration', function() {
            var stamp = Date.UTC(2011, 11, 25);
            var result = helper.calculateStampPlusDuration(stamp, 'P1Y-2M-20D');
            assert.equal(result, '05.10.2012');
        });

        it('should calculateGostPlusDuration', function() {
            var gost = '25.12.2011';
            var result = helper.calculateGostPlusDuration(gost, 'P1Y-2M-20D');
            assert.equal(result, '05.10.2012');
        });
    });

    provide();
});
