modules.define('duration-helper', ['dates'], function(provide, dates) {
    var helper = {};

    // from https://github.com/moment/moment/blame/develop/src/lib/duration/create.js#L13
    // without weeks and time, like 'P1Y-2M4D'
    var isoRegexDuration = /^P(?:(-?[0-9,.]*)Y)?(?:(-?[0-9,.]*)M)?(?:(-?[0-9,.]*)D)?$/;
    var parseIsoDuration = function(input) {
        var match = isoRegexDuration.exec(input);
        if (!match) { throw new Error('required_right_format_of_duration:' + input); }
        return {
            years : parseInt(match[1], 10),
            months : parseInt(match[2], 10),
            days : parseInt(match[3], 10)
        };
    };

    /**
     * Calculates a new date by adding a duration to the date
     * According UTC (no local time)
     * @private
     * @param {Number} stamp UTC timestamp
     * @param {String} isoDuration P1Y, P6M, etc.
     * @returns {String} A duration (year, month) later, GOST format
     */
    helper.calculateStampPlusDuration = function(stamp, isoDuration) {
        if (stamp === null || isoDuration === null) { return null; }

        var d = new Date(stamp);

        var duration = parseIsoDuration(isoDuration);

        if (duration.years) {
            d.setUTCFullYear(d.getUTCFullYear() + duration.years);
        }

        if (duration.months) {
            d.setUTCMonth(d.getUTCMonth() + duration.months);
        }

        if (duration.days) {
            d.setUTCDate(d.getUTCDate() + duration.days);
        }

        return dates.fromDateToGost(d);
    };

    helper.calculateGostPlusDuration = function(gost, isoDuration) {
        if (gost === null || isoDuration === null) { return null; }

        return helper.calculateStampPlusDuration(dates.fromGostToStamp(gost), isoDuration);
    };

    provide(helper);
});
