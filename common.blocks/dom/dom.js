modules.define('dom', ['objects'],
function (provide, Objects, DOM) {

    //быстрая замена всех элементов внутри DOM-node
    var replaceChildren = function (node, fragment) {
        var newNode = node.cloneNode(false);
        newNode.appendChild(fragment);
        node.parentNode.replaceChild(newNode, node);
    }

    provide(Objects.extend(DOM, { replaceChildren: replaceChildren }));
}
);
