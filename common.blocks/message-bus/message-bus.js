/**
 * Класс обмена сообщениями. Ядро системы.
 *
 * Приложение создает два глобальных экземпляра:
 *      request-bus  -- сообщения-команды отправляемые из view-presenters для models
 *      response-bus -- сообщения о событиях проиcходящих в models отправляются во view-presenters
 **/
modules.define('message-bus', function(provide) {

    /**
     * @param instanceName необязательное имя конкретного экземпляра, нужно только для отладочных console.log() сообщений
     **/
    var MessageBus = function(instanceName) {
        this.name = instanceName;
        this.subscribers = [];
    };

    /**
     * Отправка сообщения всем ранее зарегистированным подписчикам
     *
     * @param message object само сообщение
     **/
    MessageBus.prototype.send = function(message) {
        this.name && console.log(this.name + '.send', message);

        this.subscribers.forEach( function(subscriber) {
            //все подписчики уведомляются параллельно
            setTimeout( function() {
                subscriber(message);
            }, 0);
        });
    };

    /**
     * Регистрация подписчика
     *
     * @param subscriber function(message) функция-получатель сообщения
     * @return ключ данного подписчика, по которому можно потом отписать себя
     **/
    MessageBus.prototype.subscribe = function(subscriber) {
        var subscriberKey = this.subscribers.push(subscriber);
        this.name && console.log(this.name + '.subscribe', subscriberKey);
        return subscriberKey;
    };

    /**
     * отмена регистрации подписчика
     **/
    MessageBus.prototype.unsubscribe = function(subscriberKey) {
        this.name && console.log(this.name + '.unsubscribe', subscriberKey);

        /**
         * заменяем функцию подписки на пустую, чтобы индексы массива не изменились
         **/
        var subscriberIndex = subscriberKey - 1;
        this.subscribers[subscriberIndex] = function() {};
    };

    provide(MessageBus);

});
