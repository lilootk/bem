modules.define('presenter', ['request-bus', 'response-bus', 'objects', 'arrays'],
function(provide, requestBus, responseBus, Objects, Arrays) {

    var Presenter = function() {
        this.isInited = false;
    };

    Presenter.prototype.requestBus = requestBus;
    Presenter.prototype.responseBus = responseBus;

//public:
    Presenter.prototype.subscribeBlock = function(block, writeKeys, readKeys) {
        if (!block || !block.setProps || !block.findBlockOutside) {
            //фатальная ошибка, т.к. дальнейшая работа презентера невозможна
            throw 'Ожидается правильный БЭМ-блок';
        }

        this.block = block;

        /**
         * свойства, которые данный блок имеет право изменять
         **/
        this.writeKeys = writeKeys || [];

        /**
         * все свойства, которые нужны блоку для своего отображения
         **/
        this.blockKeys = [].concat(this.writeKeys, readKeys || []);

        /**
         * пытаемся найти блок типа view, к которому принадлежит данный компонент
         * компонент считается невидимым в данный момент, если его view не является текущим
         **/
        var view = this.block.findBlockOutside('view');

        /**
         * блока-хозяина view может не быть, кроме того, могут существовать view блоки без параметра url,
         * в обоих случаях данный блок будет перерисовываться сразу же при любым изменениям своих свойств
         **/
        var viewUrl = view && view.getUrl();
        if (!viewUrl) {
            this.responseBus.subscribe(this.responseSimple.bind(this));
        }
        else {
            this.viewUrl = viewUrl;
            this.changedKeys = [];
            this.responseBus.subscribe(this.responseViewAware.bind(this));
        }

        //initial request
        this.requestBus.send({cmd: 'ping'});
    };

    Presenter.prototype.sendProps = function(changedProps) {
        var blockProps = Objects.only(changedProps, this.writeKeys);
        this.requestBus.send({cmd: 'set', value: blockProps});
    };

    Presenter.prototype.sendProp = function(key, value) {
        var changedProps = {};
        changedProps[key] = value;
        this.sendProps(changedProps);
    };

    Presenter.prototype.command = function(cmd, key, value) {
        this.requestBus.send({cmd: cmd, key: key, value: value});
    };

//protected:
    Presenter.prototype.setBlockProps = function(state, changedKeys) {
        var blockProps = Objects.only(state, this.blockKeys);

        console.log('setProps', { block: this.block, blockProps: blockProps, changedKeys: changedKeys });
        this.block.setProps(blockProps, changedKeys);
    };

    /**
     * функция для самой первой отрисовки
     **/
    Presenter.prototype.initBlockProps = function(state, changedKeys) {
        this.isInited = true;
        var blockProps = Objects.only(state, this.blockKeys);

        console.log('initProps', { block: this.block, blockProps: blockProps, changedKeys: changedKeys });
        if (this.block.initProps) {
            this.block.initProps(blockProps, changedKeys);
        }
        else {
            this.block.setProps(blockProps, changedKeys);
        }
    };

    Presenter.prototype.responseSimple = function(response) {
        /**
         * начальная отрисовка должна произойти независимо от наличия каких-то изменений
         **/
        if (!this.isInited) {
            this.initBlockProps(response.state, this.blockKeys);
            return;
        }

        var changedKeys = Arrays.only(response.changedKeys, this.blockKeys);
        if (changedKeys.length) {
            this.setBlockProps(response.state, changedKeys);
            return;
        }

    };

    /**
     * оптимизированный способ отрисовки:
     * перерисовываем только блоки в пределах текущего (видимого) блока view
     **/
    Presenter.prototype.responseViewAware = function(response) {

        /**
         * накапливаем изменения, чтобы потом разом всё обновить
         **/
        var changedKeys = Arrays.only(response.changedKeys, this.blockKeys);
        this.changedKeys = Arrays.merge(this.changedKeys, changedKeys);

        if (this.viewUrl !== response.state.viewUrl) {
            /**
             * если блок мы считаем невидимым в данный момент, то пока он не станет видимым,
             * никаких действий по его отрисовке производить не будем
             **/
            return;
        };

        /**
         * начальная отрисовка должна произойти независимо от наличия каких-то изменений
         **/
        if (!this.isInited) {
            this.initBlockProps(response.state, this.blockKeys);
            this.changedKeys = [];
            return;
        }

        if (this.changedKeys.length) {
            this.setBlockProps(response.state, this.changedKeys);
            this.changedKeys = [];
            return;
        }
    };

    provide(Presenter);
}
);
