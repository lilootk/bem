modules.define('state', ['objects'],
function(provide, Objects) {

    var clone = Objects.clone;

    var State = function() {
        this.state = {};
        this.subscribers = [];
    };

    State.prototype.getState = function() {
        return clone(this.state);
    };

    /**
     * Для каждого одноименного собственного свойства из object
     * создаем или полностью заменяем свойства внутреннего состояния _state
     **/
    State.prototype.setMany = function(object) {
        var newState = this.getState();
        var changedKeys = [];

        Objects.each(object, function(value, key) {
            if (newState[key] !== value) {
                newState[key] = clone(value);
                changedKeys.push(key);
            }
        });

        if (!changedKeys.length) {
            /**
             * если оказалось, что фактических изменений не было,
             * то никаких событий мы не станем отправлять
             **/
            return;
        }

        /**
         * атомарно обновляем внутреннее состояние
         **/
        this.state = clone(newState);

        this.subscribers.forEach( function(subscriber) {
            subscriber(newState, changedKeys);
        });
    };

    State.prototype.pingState = function() {
        var state = this.getState();
        this.subscribers.forEach( function(subscriber) {
            subscriber(state, []);
        });
    };

    State.prototype.subscribe = function(subscriber) {
        this.subscribers.push(subscriber);
    };

    provide(new State);

});
