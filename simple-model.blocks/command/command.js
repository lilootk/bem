modules.define('command', ['arrays'],
function(provide, Arrays) {

    var apply = function(state, Func, key, value) {
        var initialArray = state[key] || [];
        var resultArray = Func(initialArray, value);

        if (resultArray.length === initialArray.length) {
            return {}; // оптимизация: если ничего не изменилось, то и не надо ничего записывать
        }

        var changedProps = {};
        changedProps[key] = resultArray;
        return changedProps;
    };

    /**
     * request - это закодированная для передачи по шине команда,
     * command - это функция, результат которой набор фактических изменений состояния
     */
    var Command = function() {
    };

    Command.prototype.props = ['cmd'];

    Command.prototype.commands = {
        set: function(command) {
            return command.value;
        },

        add: function(command, state) {
            return apply(state, Arrays.pushUnique.bind(Arrays), command.key, command.value);
        },

        remove: function(command, state) {
            return apply(state, Arrays.remove.bind(Arrays), command.key, command.value);
        }
    };

    Command.prototype.transformRequest = function(request, state) {
        var command = this.commands[request.cmd];

        if (!command) {
            console.warn('игнорируем неизвестный request:', request);
            return {};
        }

        var changedProps = command(request, state);
        return changedProps;
    };

    Command.prototype.transformResponse = function(response) {
        return response;
    };

    provide(Command);
}
);
