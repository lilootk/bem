modules.define('countries-model', ['countries-data', 'objects'],
function(provide, CountriesData, Objects) {

    var CountriesModel = function() {
    };

    CountriesModel.prototype.props = ['country'];

    CountriesModel.prototype.transformResponse = function(originalResponse) {
        var response = Objects.clone(originalResponse);
        response.state.country = (response.state.country || []).map(CountriesData.getName);
        return response;
    };

    CountriesModel.prototype.transformRequest = function(request) {
        if (request.key === 'country') {
            request.value = CountriesData.getCode(request.value);
            return request;
        };

        ['country'].forEach( function(key) {
            if (request[key] != null) {
                request[key] = request[key].map(CountriesData.getCode);
            }
        });

        return request;
    };

    provide(CountriesModel);

}
);
