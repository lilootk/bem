modules.define('state-persistent', ['state', 'request-bus', 'localforage', 'functions__debounce' ],
function(provide, stateStorage, requestBus, persistentStorage, debounce) {

    /**
     * эта функция будет выполняться на каждый response oт модели,
     * поэтому мы защитим её с помощью debounce() от спама localforage слишком частыми запросами
     **/
    var saveModelState = debounce( function(state) {
        console.log('save model', state);
        persistentStorage.setItem('model', state);
    }, 100);

    var init = function() {
        persistentStorage.getItem('model', function(err, value) {
            /**
             * если в localforage пусто, то мы НЕ хотим выполнять request на изменение состояния модели на пустое
             * ведь кто-то другой может инициализировать модель на более полезное начальное состояние, чем пустое
             **/
            if (value) {
                requestBus.send({ cmd: 'set', value: value });
            }

            /**
             * подписываемся на выполнение сохранений состояния лишь после того,
             * как сами предварительно отправили первый request с состоянием из localforage
             * иначе может получиться, что мы затрем сохранённое в localforage до того, как успели его загрузить в модель
             **/
            stateStorage.subscribe(saveModelState);
        });
    };

    init();
    provide();

});
