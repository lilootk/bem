modules.define('view', ['i-bem__dom'],
function(provide, BEMDOM) { provide(BEMDOM.decl(this.name, {

    getUrl: function() {
        return this.params.url;
    },

    getParams: function() {
        return this.params;
    }

}));});
