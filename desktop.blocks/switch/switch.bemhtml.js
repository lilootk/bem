block('switch')(
    js()(true),
    tag()('label'),
    content() ( function() {
        return [
            {
                elem: 'checkbox',
                tag: 'input',
                // NOTE: don't remove autocomplete attribute, otherwise js and DOM may be desynced
                attrs: { type:'checkbox', autocomplete : 'off' }
            },
            {
                elem: 'handle',
                tag: 'span'
            },
            {
                elem: 'label',
                tag: 'span',
                content: this.ctx.label
            }
        ]
    })

);
