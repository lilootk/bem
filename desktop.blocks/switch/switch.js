modules.define('switch', ['i-bem__dom'],
function(provide, BEMDOM) { provide(BEMDOM.decl(this.name, {
    onSetMod : {
        'checked' : {
            'true' : function() {
                this.elem('checkbox')
                    .attr('checked', true)
                    .prop('checked', true);
            },
            '' : function() {
                this.elem('checkbox')
                    .removeAttr('checked')
                    .prop('checked', false);
            }
        }
    },

    _onChange : function() {
        this.setMod('checked', this.elem('checkbox').prop('checked'));
    }
},
{
    live: function () {
        this.liveBindTo('checkbox', 'change', this.prototype._onChange);
    }
}
))});
