block('view-button')(
    content()(function() {
        var content = {
            mix: { block: 'view-button', elem: 'text' },
            block: 'link',
            url: this.ctx.url || '#', //значение по умолчанию, если истории нет
            content: this.ctx.content
        };

        if (this.mods['history-back']) {
            content.mods = { 'history-back': true };
        }

        return content;
    })
);
