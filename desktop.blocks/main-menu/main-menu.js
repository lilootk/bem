modules.define('main-menu', ['i-bem__dom'],
function(provide, BEMDOM) { provide(BEMDOM.decl(this.name, {

    onSetMod: {
        'js': {
            'inited': function () {
                this.bindToWin('click', function () {
                    this.delMod('visible');
                });
            }
        }
    },

},
{
    live: function () {
        this.liveBindTo('icon', 'click', function (e) {
            this.toggleMod('visible');
            e.stopPropagation();
        });
    }
}
))});
