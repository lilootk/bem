block('countries-select-button').replace()( function() {
    return {
        block:'view-button',
        url: this.ctx.url,
        content: this.ctx.none,
        mix: {
            block: 'countries-select-button',
            js: function() {
                var params = {};

                ['any', 'none'].forEach( function (param) {
                    if (this.ctx[param] != null) {
                        params[param] = this.ctx[param];
                    }
                }.bind(this));

                return params;
            }.bind(this)()
        },
    };
});
