modules.define('countries-select-button', ['presenter', 'i-bem__dom'],
function(provide, Presenter, BEMDOM) { provide(BEMDOM.decl(this.name, {

    propKey: 'country',

    onSetMod: {
        js: {
            inited: function () {
                this.buttonElem = this.findBlockOn('view-button').elem('text').get(0);

                this.presenter = new Presenter();
                this.presenter.subscribeBlock(this, [], [this.propKey]);
            }
        }
    },

    setProps: function (props) {
        var buttonText = (props[this.propKey] && props[this.propKey].length) ? this.params.any : this.params.none;
        this.buttonElem.textContent = buttonText;
    }

}));}
);
