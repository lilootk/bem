modules.define('datepicker', [ 'i-bem__dom', 'pickadate' ],
function (provide, BEMDOM) { provide(BEMDOM.decl(this.name, {

    onSetMod: {
        js: {
            inited: function () {
                this.pickadate = this.domElem.pickadate().pickadate('picker');

                this.pickadate.on('set', function (data) {
                    if (data.clear !== undefined || data.select !== undefined) {
                        this.emit('select', this.get('select'));
                    }
                }.bind(this));
            }
        },

        disabled: {
            '*': function () {
                this.pickadate.$node.prop('disabled', this.hasMod('disabled'));
            }
        }
    },

    // http://amsul.ca/pickadate.js/api/#method-get-select
    get: function (key) {
        var formatSubmit = this.pickadate.component.settings.formatSubmit;
        return this.pickadate.get(key, formatSubmit);
    },

    set: function (props) {
        if (props.disabled != null) {
            this.setMod('disabled', props.disabled === true);
            delete props.disabled;
        }
        console.log('pickadate set', props);
        this.pickadate.set(props, { muted: true } );
        this.pickadate.set('select', props.select, { muted: true } ); //одновременно всё установить не получается
        return this;
    }

}));});
