block('datepicker')(
    js()(true),
    tag()('input'),
    attrs()(function() {
        return {
            placeholder: this.ctx.placeholder
        };
    })
);
