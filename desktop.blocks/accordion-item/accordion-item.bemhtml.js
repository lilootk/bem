block('accordion-item')(
    js()(true),
    content()( function() {
        return [
            { elem: 'title', content: this.ctx.title },
            { elem: 'content', content: this.ctx.content }
        ];
    })
);
