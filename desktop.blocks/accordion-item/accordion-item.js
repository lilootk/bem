modules.define('accordion-item', ['i-bem__dom'],
function(provide, BEMDOM) { provide(BEMDOM.decl(this.name, {
},
{
    live: function () {
        this.liveBindTo('title', 'click', function (e) {
            this.toggleMod('active');
        });
    }
}
))});
