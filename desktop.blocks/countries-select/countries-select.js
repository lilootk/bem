modules.define('countries-select', ['presenter', 'countries-data', 'arrays', 'dom', 'functions__debounce', 'i-bem__dom', 'BEMHTML'],
function(provide, Presenter, Data, Arrays, DOM, debounce, BEMDOM, BEMHTML) { provide(BEMDOM.decl(this.name, {

    propKey: 'country',

    filteredCountries: [], //отфильтрованные текстовым вводом страны к выбору (изначально все страны)
    selectedCountries: [], //уже выбранные страны иключаются из выбора (изначально ничего)

    onSetMod: {
        js: {
            inited: function() {
                this.specialKeys = {
                    '13': function() { this.submit(this.inputTextBlock.getVal()); }.bind(this),
                    '38': function() { console.log('prev'); },
                    '40': function() { console.log('next'); }
                };
                this.debounceSetFilter = debounce( function() {
                    this.setFilter(this.inputTextBlock.getVal());
                }.bind(this), 100);

                this.inputTextBlock = this.findElem('text').bem('input');
                this.inputTextBlock.domElem.on('keyup', this.onKeyUp.bind(this));

                this.lastText = '';
                this.filteredCountries = Data.getFilteredCountries(this.lastText);
                this.selectedCountries = [];

                this.presenter = new Presenter();
                this.presenter.subscribeBlock(this, [this.propKey]);
            }
        }
    },

    setProps: function(props) {
        this.selectedCountries = props[this.propKey] || [];
        this.render();
    },

    render: function() {
        var visibleCountries = Arrays.diff(this.filteredCountries, this.selectedCountries);
        var domFragment = this.__self.getSelectElems(visibleCountries);
        DOM.replaceChildren(this.findElem('countries-wrapper').get(0), domFragment);
    },

    onKeyUp: function(e) {
        var key = (e.keyCode || e.which);
        if (this.specialKeys[key]) {
            return this.specialKeys[key](e);
        }

        this.debounceSetFilter();
    },

    clearText: function() {
        this.inputTextBlock.setVal('');
    },

    submit: function(text) {
        this.lastText = text;
        this.filteredCountries = Data.getFilteredCountries(this.lastText);
        var visibleCountries = Arrays.diff(this.filteredCountries, this.selectedCountries)

        if (visibleCountries.length === 1) {
            var countryName = visibleCountries[0];
            this.selectCountry(countryName);
            return;
        }

        this.render();
    },

    setFilter: function(text) {
        if (text === this.lastText) {
            return;
        }
        this.lastText = text;
        this.filteredCountries = Data.getFilteredCountries(this.lastText);
        this.render();
    },

    selectCountry: function (countryName) {
        this.lastText = '';
        this.filteredCountries = Data.getFilteredCountries(this.lastText);
        this.presenter.command('add', this.propKey, countryName);
        this.clearText();
    }

},
{
    _selectElems: [], //статическая, т.к. достаточно синглетона для всех таких блоков

    createSelectElems: function() {
        //временный DOM element, т.к. fragment не имеет свойства innerHTML
        var element = document.createElement('div');

        var selectElems = {};

        Data.getAllCountriesGroups().forEach( function(country) {
            var countryName = country.name;
            var block = {
                block: 'countries-select', elem: 'country',
                elemMods: { group: country.group },
                content: countryName
            };
            element.innerHTML = BEMHTML.apply(block);
            selectElems[countryName] = element.firstChild;
        });

        return selectElems;
    },

    getSelectElems: function(countries) {
        var fragment = document.createDocumentFragment();

        countries.forEach( function(countryName) {
            var countryElement = this._selectElems[countryName];
            countryElement && fragment.appendChild(countryElement);
        }.bind(this));

        return fragment;
    },

    live: function () {
        this._selectElems = this.createSelectElems();

        this.liveBindTo('country', 'click', function (e) {
            var countryName = e.target.textContent.trim();
            this.selectCountry(countryName);
        });

        return false;
    }
}
));});
