block('countries-select')(
    js()(true),
    content()([
        {
            block: 'input',
            mix: { block: 'countries-select', elem: 'text' }
        },
        { elem: 'countries-wrapper' }
    ])
);
