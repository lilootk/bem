modules.define('main', ['presenter', 'i-bem__dom'],
function(provide, Presenter, BEMDOM) { provide(BEMDOM.decl(this.name, {

    propKey: 'viewUrl',

    views: [], //массив блоков view, c url или без (например: попапы)
    pages: {}, //блоки view с параметром url

    onSetMod: {
        'js': {
            'inited': function() {
                this.views = [];
                this.pages = {};
                this.defaultTitle = window.document.title;

                var views = this.findBlocksInside('view');
                if (!views || !views.length) {
                    return;
                }
                this.views = views;

                views.forEach( function (view) {
                    var url = view.getUrl();

                    if (url && !this.pages[url]) {
                        this.pages[url] = view;
                    }
                }.bind(this));

                this.presenter = new Presenter();
                this.presenter.subscribeBlock(this, [this.propKey]);
            }
        }
    },

    initProps: function(props) {
        var onHashChange = function() {
            this.presenter.sendProp(this.propKey, window.location.hash);
        }.bind(this);
        this.bindToWin('hashchange', onHashChange);

        if (props[this.propKey] === window.location.hash) {
            this.setProps(props);
        }
        else {
            onHashChange();
        }
    },

    setProps: function(props) {
        var url = props[this.propKey];

        var view = this.pages[url];
        if (!view) {
            /* Соглашение:
                первый view является страницей по умолчанию (с пустым url)
                последний view вызывается для обработки всех остальных неизвестных url
            */
            var i = (!url || url === '#') ? 0 : (this.views.length-1);
            view = this.views[i];
        }

        this.views.forEach( function(otherView) {
            (otherView !== view) && otherView.delMod('visible');
        });

        this.setMainTheme( view.getParams() );
        view.setMod('visible');

        //всё отрисовали, теперь, возможно, url сменится (без видимых изменений)
        var url = view.getUrl();
        (url != null) && (window.location = url);
    },

    setMainTheme: function(props) {
        window.document.title = props.title || this.defaultTitle;

        //цвет фона и прочие стили
        this.setMod('theme', (props.theme || false));

        //по умолчанию header показываем
        this.setMod(this.elem('header'), 'hidden', (props.header === false));
    }

}))});
