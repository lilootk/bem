modules.define('input-date', ['presenter', 'i-bem__dom'],
function(provide, Presenter, BEMDOM) { provide(BEMDOM.decl(this.name, {

    onSetMod: {
        js: {
            inited: function () {
                this.propKey = this.params.prop;
                this.datepicker = this.findBlockInside('datepicker');

                this.presenter = new Presenter();
                this.presenter.subscribeBlock(this, [this.propKey], this.params.propsExtra);
            }
        }
    },

    initProps: function(props) {
        this.datepicker.on('select', function (e, dateable) {
            this.presenter.sendProp(this.propKey, dateable);
        }.bind(this));

        this.setProps(props);
    },

    setProps: function (props) {
        this.datepicker.set(props[this.propKey]);
    }

}));});
