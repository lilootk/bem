block('input-date')(
    js()(function() {
        var params = {};

        ['prop', 'propDisable', 'propsExtra'].forEach( function (param) {
            if (this.ctx[param] != null) {
                params[param] = this.ctx[param];
            }
        }.bind(this));

        return params;
    }),
    content()(function() {
        return [
            {
                elem: 'wrap',
                content: [
                    {
                        elem: 'label',
                        content: this.ctx.label || 'Дата'
                    },
                    {
                        mix: {block: this.ctx.block, elem: 'picker'},
                        block: 'datepicker',
                        placeholder: this.ctx.placeholder || this.ctx.label || 'Дата'
                    }
                ]
            }
        ];
    })
);
