block('countries-selected')(
    js()(true),
    content()(
        { elem: 'countries-wrapper' }
    )
)
