modules.define('countries-selected', ['presenter', 'BEMHTML', 'i-bem__dom'],
function(provide, Presenter, BEMHTML, BEMDOM) { provide(BEMDOM.decl(this.name, {

    propKey: 'country',

    onSetMod: {
        js: {
            inited: function () {
                this.presenter = new Presenter();
                this.presenter.subscribeBlock(this, [this.propKey]);
            }
        }
    },

    setProps: function (props) {
        var bemjson = (props[this.propKey] || []).map( function (countryName) {
            return {
                block: 'countries-selected', elem: 'country',
                content: [
                    { elem: 'name', content: countryName },
                    { elem: 'disable' }
                ]
            };
        });
        this.elem('countries-wrapper').get(0).innerHTML = BEMHTML.apply(bemjson);
    }

},
{
    live: function () {
        this.liveBindTo('disable', 'click', function (e) {
            var countryName = e.target.parentNode.textContent.trim();
            this.presenter.command('remove', this.propKey, countryName);
        });

        return false;
    }
}

));}
);
