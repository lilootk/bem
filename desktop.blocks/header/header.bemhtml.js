block('header')(
    tag()('header'),
    mix()(['clearfix', { block: 'main', elem: 'header' }]),
    content()({
        elem: 'inner',
        content: [
            {
                elem: 'menu-icon',
                mix: {block: 'main-menu', elem: 'icon'},
                content: {
                    block: 'icon',
                    name: 'info'
                }
            },
            {
                elem: 'logo',
                tag: 'a',
                attrs: {href: '#home'},
            },
            {
                elem: 'phone',
                tag: 'a',
                attrs: {href:'tel:8(800)5552198'},
                content: {
                    block: 'icon',
                    name: 'phone'
                }
            }
        ]
    })
);
