block('input-switch')(
    js()(function() {
        var params = {};

        ['prop', 'propDisable', 'propsExtra'].forEach( function (param) {
            if (this.ctx[param] != null) {
                params[param] = this.ctx[param];
            }
        }.bind(this));

        return params;
    }),
    content()(function() {
        return {
            mix: {block: 'multipolicy', elem: 'checkbox'},
            block: 'switch',
            label: this.ctx.label || this.ctx.prop,
        };
    })
);
