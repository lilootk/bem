modules.define('input-switch', ['presenter', 'i-bem__dom'],
function(provide, Presenter, BEMDOM) { provide(BEMDOM.decl(this.name, {

    onSetMod: {
        js: {
            inited: function() {
                this.checkboxBlock = this.findBlockInside('switch');
                this.propKey = this.params.prop;

                this.presenter = new Presenter();
                this.presenter.subscribeBlock(this, [this.propKey]);
            }
        }
    },

    initProps: function(props) {
        this.checkboxBlock.on({ modName: 'checked', modVal: '*' }, function() {
            this.presenter.sendProp(this.propKey, this.checkboxBlock.hasMod('checked'));
        }.bind(this));

        this.setProps(props);
    },

    setProps: function (props) {
        var checked = props[this.propKey] ? true : false;
        this.checkboxBlock.setMod('checked', checked);
    }

}));});
