modules.define('link', ['i-bem__dom'], function(provide, BEMDOM) {

    provide(BEMDOM.decl(this.name, {})); // Объявляем базовый блок

});

// Доопределяем базовый блок с модификатором history-back
modules.define('link', function(provide, Base) { provide(Base.decl({ modName : 'history-back' }, {

},
{
    live: function () {
        this.liveBindTo('click', function (e) {
            if (window.history && window.history.length) {
                window.history.back();
            }
            else {
                window.location = this.findBlockInside('link').elem('control').attr('href');
            }
        });
    }
}
));

});
