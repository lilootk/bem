modules.define('pickadate', ['jquery'],
function (provide, jQuery) {
    /**
     * http://amsul.ca/pickadate.js/date/
     */
    provide(
        (function() {
            // eslint-disable-line no-unused-vars
            /*borschik:include:../../libs/pickadate/lib/picker.js*/
            /*borschik:include:../../libs/pickadate/lib/picker.date.js*/

            jQuery.extend(jQuery.fn.pickadate.defaults, {
                format: 'dd.mm.yyyy',
                formatSubmit: 'dd.mm.yyyy',
                monthsFull: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
                weekdaysShort: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Вс'],
                today: 'Сегодня',
                clear: '',
                close: 'Закрыть',
                labelMonthNext: 'Следующий месяц',
                labelMonthPrev: 'Предыдущий месяц'
            });
        }())
    );

});
